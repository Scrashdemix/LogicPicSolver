from tkinter import Text, Tk


class Info(Text):

    def __init__(self, window: Tk, is_column: bool, info: tuple):
        height = 1
        if is_column:
            height = len(info)
        super(Info, self).__init__(window, height=height)
