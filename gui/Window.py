import tkinter

from Table import Table
from gui.infos import Info


class Window(tkinter.Tk):

    def __init__(self, table: Table):
        super(Window, self).__init__()
        height = table.row_nr * 25
        width = table.col_nr * 30
        self.geometry(str(width) + "x" + str(height))
        self.table = table
        self.build()

    def build(self):
        for i in range(self.table.row_nr):
            if i == 0:
                pass  # TODO set column info
                Info(self, True, self.table.col_info[i])
            for j in range(self.table.col_nr):
                if j == 0:
                    pass  # TODO set row info
                temp = tkinter.Checkbutton(self)
                temp.grid(row=i, column=j)
                if self.table.solution[i][j] is True:
                    temp.select()
