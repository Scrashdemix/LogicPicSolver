from Table import Table


class Solver:
    def __init__(self, rows, columns):
        self.rows = rows
        self.columns = columns
        self.table = Table(rows, columns)

    def solve(self):
        solved = False
        while not solved:
            for i in range(len(self.rows)):
                self.solve_row(self.table.solution[i])
            for j in range(len(self.columns)):
                self.solve_column(j)

    def solve_row(self, row_number: int):
        # copy current row from solution
        row = self.table.solution[row_number]
        if self.table.check_row(row, row_number):
            for i in range(0, row):
                if row[i] is None:
                    row[i] = False
            return
        # try every possible solution and add probabilities to every field
        new = self.try_every_possibility(row)
        # set results in solution
        for i in range(0, len(new)):
            if new[i] is True:
                row[i] = True
            elif new[i] is False:
                row[i] = False
        # set new row in solution
        self.table.solution[row_number] = row

    def solve_column(self, column_number: int):
        # make temporary column from solution
        col = self.table.make_temp_column(column_number)
        # try every possible solution and add probabilities to every field
        new = self.try_every_possibility(col)
        #  set results in solution
        for i in range(0, len(new)):
            if new[i] is True:
                col[i] = True
            elif new[i] is False:
                col[i] = False
        # set new column in solution
        for i in range(0, self.table.col_len):
            self.table.solution[i][column_number] = col

    def try_every_possibility(self, line: list, info: tuple) -> list:
        """
        Try every possibility of a row or column and create list of possibilities
        :param line: row or column
        :return: list of possibilities
        """
        # setup list of possibilities
        poss = [None for _ in range(0, len(line))]
        for i in range(0, len(line)):
            if line[i] is True:
                poss[i] = 1
            elif line[i] is False:
                poss[i] = 0
        # TODO Calculate
        return poss
