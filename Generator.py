from random import choice


def generate(width: int, height: int):
    solution = [[None for _ in range(width)] for _ in range(height)]
    row_info = []
    for i in range(height):
        for j in range(width):
            solution[i][j] = choice((True, False))  # generate field
        # generate row_info
        this_row_info = []
        index = 0
        for j in range(width):
            field = solution[i][j]
            if field is True:
                index += 1
            else:  # field is False
                if index > 0:
                    this_row_info.append(index)
                    index = 0
        if index != 0:
            this_row_info.append(index)
        row_info.append(tuple(this_row_info))
    # generate col_info
    col_info = []
    for j in range(width):
        this_col_info = []
        index = 0
        for i in range(height):
            field = solution[i][j]
            if field is True:
                index += 1
            else:  # field is False
                if index > 0:
                    this_col_info.append(index)
                    index = 0
        if index != 0:
            this_col_info.append(index)
        col_info.append(tuple(this_col_info))

    return solution, tuple(row_info), tuple(col_info)
