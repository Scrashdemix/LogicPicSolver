from unittest import TestCase
from random import randrange

from Solver import Solver
from Table import Table
from Generator import generate
from gui.Window import Window


class UnitTest(TestCase):

    @staticmethod
    def data_for_tests():
        rows = (
            (1,),
            (1,),
            (5,),
            (5,),
            (5,)
        )

        columns = (
            (3,),
            (3,),
            (3,),
            (5,),
            (3,)
        )
        return rows, columns

    def test_setup(self):
        # setup
        rows, columns = self.data_for_tests()
        t = Table(rows, columns)
        # test setup
        self.assertEqual(rows, t.row_info)
        self.assertEqual(columns, t.col_info)
        self.assertEqual(5, len(t.solution))
        self.assertEqual(5, len(t.solution[0]))
        self.assertEqual(5, len(t.solution[1]))
        self.assertEqual(5, len(t.solution[2]))
        self.assertEqual(5, len(t.solution[3]))
        self.assertEqual(5, len(t.solution[4]))

    def test_check_row(self):
        rows, columns = self.data_for_tests()
        t = Table(rows, columns)
        t.solution[0] = [None, True, False, False, None]
        t.solution[1] = [True, True, True, True, None]
        self.assertTrue(t.check_row(t.solution[0], 0))
        self.assertFalse(t.check_row(t.solution[1], 1))

    def test_check_column(self):
        rows, columns = self.data_for_tests()
        t = Table(rows, columns)
        t.solution[0][1] = True
        t.solution[1][1] = True
        t.solution[2][1] = True
        self.assertTrue(t.check_column(t.make_temp_column(1), 1))

    def test_temp_column(self):
        rows, columns = self.data_for_tests()
        t = Table(rows, columns)
        t.solution[0][1] = True
        t.solution[1][1] = True
        t.solution[2][1] = True
        temp = t.make_temp_column(1)
        self.assertEqual([True, True, True, None, None], temp)

    def test_possibilities(self):
        rows, columns = self.data_for_tests()
        s = Solver(rows, columns)
        s.try_every_possibility([True])


class TestGenerator(TestCase):

    def test_generator(self):
        height = randrange(5, 20)
        width = randrange(5, 20)
        solution, row_info, col_info = generate(width, height)
        table = Table(row_info, col_info)
        table.solution = solution
        for i in range(height):
            self.assertTrue(table.check_row(solution[i], i))
        for j in range(width):
            self.assertTrue(table.check_column(table.make_temp_column(j), j))


class TestWindow(TestCase):

    def test_window(self):
        height = randrange(5, 20)
        width = randrange(5, 20)
        sol, rows, cols = generate(width, height)
        tab = Table(rows, cols)
        tab.solution = sol
        win = Window(tab)
        win.mainloop()
