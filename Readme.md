# LogicPic Solver
This project is used as a playground for using artificial neural networks to solve tasks of the mobile game [LogicPic](https://play.google.com/store/apps/details?id=br.com.tapps.logicpic&hl=en_US&gl=US). The ANN is trained using generated data.

> This project is deprecated.
