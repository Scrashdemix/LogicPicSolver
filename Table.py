
#     | | | | | <- col_info
# --- X X X X X
# --- X X X X X
# --- X X X X X
# --- X X X X X
# --- X X X X X
# ^
# |
# row_info


class Table:

    def __init__(self, row_info, col_info):
        """
        New empty table as playground.
        :param row_info: info of each row
        :param col_info: info of each column
        """
        # None: empty field
        # False: blue X
        # True: black field

        # rows are lists of columns
        # rows are always read from left
        # columns are always read from top

        self.row_info = row_info  # info of each row
        self.col_info = col_info  # info of each column
        self.row_len = len(col_info)  # length of rows
        self.col_len = len(row_info)  # length of columns
        self.row_nr = self.col_len  # number of rows
        self.col_nr = self.row_len  # number of columns
        self.solution = [[None for _ in range(self.col_nr)] for _ in range(self.row_nr)]

        # checks for possible infos
        row_lengths = [self.needed_len(row_info[i]) for i in range(0, len(row_info))]
        assert self.row_len >= max(row_lengths)
        col_lengths = [self.needed_len(col_info[i]) for i in range(0, len(col_info))]
        assert self.col_len >= max(col_lengths)

    def __str__(self):
        string = ""
        for i in range(0, self.row_len):
            for j in range(0, self.col_len):
                string += str(self.solution[i][j])
                if j != (self.row_len - 1):
                    string += ", "
            string += "\n"
        return string

    def check_row(self, row: list, row_info_number: int) -> bool:
        """
        Checks row like original program (row becomes blue in original)
        :param row: row to check
        :param row_info_number: row_info to be checked
        :return: True iff row could be right, else False
        """
        row_info = self.row_info[row_info_number]
        info_index = 0  # index which block is next
        currentblock = 0
        numberofblocks = 0
        for column in range(len(row)):
            a = row[column]
            if a is True:  # field is black
                currentblock += 1
            elif a is False or a is None:  # field is blue or white
                if currentblock != 0:
                    if not currentblock == row_info[info_index]:
                        return False
                    info_index += 1
                    currentblock = 0
                    numberofblocks += 1
            else:
                raise AssertionError("Feld ist weder True noch False oder None, nämlich {}".format(a))
            if column == (len(row) - 1):  # last field
                if currentblock != 0:
                    numberofblocks += 1
                    if not currentblock == row_info[info_index]:
                        return False
                if numberofblocks != len(row_info):
                    return False
        return True

    def check_column(self, column: list, col_info_number: int) -> bool:
        """
        Checks column like original program (column becomes blue in original)
        :param column: column to check
        :param col_info_number: number of col_info to be checked
        :return: True iff column could be right, else False
        """
        col_info = self.col_info[col_info_number]
        info_index = 0
        currentblock = 0
        numberofblocks = 0
        for row in range(len(self.solution)):
            a = column[row]
            if a is True:  # field is black
                currentblock += 1
            elif a is False or a is None:  # field is blue or white
                if currentblock != 0:
                    if not currentblock == col_info[info_index]:
                        return False
                    info_index += 1
                    currentblock = 0
                    numberofblocks += 1
            else:
                raise AssertionError("Feld ist weder True noch False oder None, nämlich {}".format(a))
            if row == (len(self.solution) - 1):  # last field
                if currentblock != 0:
                    numberofblocks += 1
                    if not currentblock == col_info[info_index]:
                        return False
                if numberofblocks != len(col_info):
                    return False
        return True

    def make_temp_column(self, col_nr: int) -> list:
        """
        Make temporary column from solution
        :param col_nr: number of column
        :return: temporary column as list
        """
        temp = []
        for row in range(0, self.col_len):
            temp.append(self.solution[row][col_nr])
        return temp

    @staticmethod
    def needed_len(info: tuple) -> int:
        """
        :param info: row_info or column_info
        :return: needed length of row or column
        """
        temp = 0
        if len(info) == 0:
            temp = 0
        elif len(info) == 1:
            temp = info[0]
        else:
            for i in range(0, len(info)):
                temp += info[i]
            temp += len(info) - 1
        return temp
